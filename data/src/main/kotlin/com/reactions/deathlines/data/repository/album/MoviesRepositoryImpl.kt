package com.reactions.deathlines.data.repository.album

import androidx.paging.PagedList
import androidx.paging.RxPagedListBuilder
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import com.reactions.deathlines.data.common.extension.applyIoScheduler
import com.reactions.deathlines.data.datasource.movie.AlbumsApiDataSource
import com.reactions.deathlines.data.datasource.movie.MovieDatabaseDataSource
import com.reactions.deathlines.data.repository.BaseRepositoryImpl
import com.reactions.deathlines.domain.common.ResultState
import com.reactions.deathlines.domain.entity.Entity
import com.reactions.deathlines.domain.repository.album.MoviesRepository
import io.reactivex.Single

class MoviesRepositoryImpl(
        private val apiSource: AlbumsApiDataSource,
        private val databaseSource: MovieDatabaseDataSource
) : BaseRepositoryImpl<Entity.Movie>(), MoviesRepository {

    override fun getApiKey(email: String): Single<ResultState<String>> {
         return apiSource.getApiKey(email).applyIoScheduler().map { apiKey ->
            if (apiKey.isEmpty())
                ResultState.Loading(apiKey)
            else
                ResultState.Success(apiKey)
        }
                .onErrorReturn { e -> ResultState.Error(e, null) }
    }

    override fun getMovies(
            sort: String,
            querySearch: String
    ): Flowable<ResultState<PagedList<Entity.Movie>>> {
        databaseSource.clearTable {  }
        val dataSourceFactory = databaseSource.getMovies()
        val boundaryCallback = MoviesRepoBoundaryCallback(apiSource, databaseSource, sort, querySearch)
        val data = RxPagedListBuilder(dataSourceFactory, DATABASE_PAGE_SIZE)
                .setBoundaryCallback(boundaryCallback)
                .buildFlowable(BackpressureStrategy.BUFFER)

        return data
                .applyIoScheduler()
                .map { d ->
                    if (d.size > 0)
                        ResultState.Success(d) as ResultState<PagedList<Entity.Movie>>
                    else
                        ResultState.Loading(d) as ResultState<PagedList<Entity.Movie>>
                }
                .onErrorReturn { e -> ResultState.Error(e, null) }
    }

    companion object {
        private const val DATABASE_PAGE_SIZE = 20
    }
}