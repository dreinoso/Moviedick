package com.reactions.deathlines.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.reactions.deathlines.data.db.album.MovieDao
import com.reactions.deathlines.data.db.album.MovieData

/**
 * Database class with all of its dao classes
 */
@Database(entities = [MovieData.Movie::class], version = 6, exportSchema = false)
abstract class MyDatabase : RoomDatabase() {

    abstract fun songDao(): MovieDao
}