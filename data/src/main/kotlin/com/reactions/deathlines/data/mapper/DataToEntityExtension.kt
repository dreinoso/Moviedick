package com.reactions.deathlines.data.mapper

import com.reactions.deathlines.data.db.album.MovieData
import com.reactions.deathlines.domain.entity.Entity

fun MovieData.Movie.map() = Entity.Movie(
        id = id.toInt(),
        title = title,
        description = description ?: "",
        image = image ?: ""
)