package com.reactions.deathlines.data.repository.album

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.paging.PagedList
import com.reactions.deathlines.data.datasource.movie.AlbumsApiDataSource
import com.reactions.deathlines.data.datasource.movie.MovieDatabaseDataSource
import com.reactions.deathlines.data.datasource.movie.getMovies
import com.reactions.deathlines.domain.common.ResultState
import com.reactions.deathlines.domain.entity.Entity

class MoviesRepoBoundaryCallback(
        private val apiSource: AlbumsApiDataSource,
        private val databaseSource: MovieDatabaseDataSource,
        private val sort: String,
        private val querySearch: String
) : PagedList.BoundaryCallback<Entity.Movie>() {

    // keep the last requested page. When the request is successful, increment the page number.
    private var firstTime = true
    private var lastRequestedPage = 1
    private val _networkErrors = MutableLiveData<String>()
    // LiveData of network errors.
    val networkErrors: LiveData<String>
        get() = _networkErrors

    // avoid triggering multiple requests in the same time
    private var isRequestInProgress = false

    /**
     * Database returned 0 items. We should send request to the backend for more items.
     */
    override fun onZeroItemsLoaded() {
        requestAndSaveData()
    }

    /**
     * When all items in the database were loaded, we need to send a request to the backend for more items.
     */
    override fun onItemAtEndLoaded(itemAtEnd: Entity.Movie) {
        requestAndSaveData()
    }

    private fun requestAndSaveData() {
        if (isRequestInProgress) {
            Log.e(this.javaClass.name, "requestAndSaveData: request in progress" )
            return
        }

        isRequestInProgress = true
        getMovies(apiSource, lastRequestedPage, sort, querySearch) { songs ->
            Log.d(this.javaClass.name, "requestAndSaveData: $songs")

            when (songs) {
                is ResultState.Success -> {

                        Log.d(this.javaClass.name, "requestAndSaveData: firstTime false")
                        databaseSource.persist(songs.data) {
                            lastRequestedPage++
                            isRequestInProgress = false
                        }
//                    }
                }
                is ResultState.Error -> {
                    Log.e(this.javaClass.name, "Something happened! ${songs.throwable.message}")
                    _networkErrors.postValue(songs.throwable.message)
                    isRequestInProgress = false
                }
            }
        }
    }
}