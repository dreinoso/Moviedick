package com.reactions.deathlines.data.mapper

import com.reactions.deathlines.data.db.album.MovieData
import com.reactions.deathlines.domain.entity.Entity

/**
 * Extension class to map album entity to album data
 */
fun Entity.Movie.map() = MovieData.Movie(
        id = id.toLong(),
        title = title,
        description = description,
        image = image
)