package com.reactions.deathlines.data.datasource.movie

import io.reactivex.Single
import com.com.reactions.deathlines.data.api.MoviesApi
import com.reactions.deathlines.data.common.extension.applyIoScheduler
import com.reactions.deathlines.data.mapper.map
import com.reactions.deathlines.domain.entity.Entity
import com.reactions.deathlines.domain.repository.PreferenceRepository

class MovieApiDataSourceImpl(
        private val api: MoviesApi,
        private val preferenceRepository: PreferenceRepository) : AlbumsApiDataSource {

    override fun getApiKey(email: String): Single<String> =
            api.getKey(email).applyIoScheduler().map { it.key }

    override fun getMovies(
            page: Int,
            sort: String,
            queryString: String
    ): Single<List<Entity.Movie>> {
        if (queryString.isBlank()) {
            return api.getMovies(preferenceRepository.getAccessToken(), page, sort)
                    .applyIoScheduler()
                    .map { item ->
                        item.filter { it.adult == false }.map { it.map() }
                    }
        } else {
            return api.getMovies(preferenceRepository.getAccessToken(), page, sort, queryString)
                    .applyIoScheduler()
                    .map { item ->
                        item.filter { it.adult == false }.map { it.map() }
                    }
        }
    }
}