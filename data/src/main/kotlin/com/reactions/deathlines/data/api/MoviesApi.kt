package com.com.reactions.deathlines.data.api

import com.google.gson.annotations.SerializedName
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Path
import retrofit2.http.Query

/**
 * Albums api
 */
interface MoviesApi {

    @GET("/key")
    fun  getKey(
            @Query("email") email: String)
            : Single<Dto.KeyResponse>

    @GET("/movies")
    fun  getMovies(
            @Header("api-key") apiKey: String,
            @Query("page") page: Int,
            @Query("sort") sort: String)
            : Single<List<Dto.MovieResponse>>

    @GET("/movies")
    fun  getMovies(
            @Header("api-key") apiKey: String,
            @Query("page") page: Int,
            @Query("sort") sort: String,
            @Query("q") q: String)
            : Single<List<Dto.MovieResponse>>

    @GET("/images")
    fun  getImage(
            @Header("api-key") apiKey: String,
            @Path("path") path: String)
            : Single<Dto.ImageResponse>

    sealed class Dto {
        data class Song(
                @SerializedName("artistId") val artistId: Long,
                @SerializedName("collectionId") val collectionId: Long,
                @SerializedName("trackId") val trackId: Long,
                @SerializedName("wrapperType") val wrapperType: String,
                @SerializedName("artistName") val artistName: String,
                @SerializedName("collectionName") val collectionName: String?,
                @SerializedName("trackName") val trackName: String,
                @SerializedName("trackPrice") val trackPrice: String,
                @SerializedName("primaryGenreName") val primaryGenreName: String,
                @SerializedName("previewUrl") val previewUrl: String,
                @SerializedName("artworkUrl100") val artworkUrl100: String
        ) : Dto()

        data class KeyResponse(
                @SerializedName("key") val key: String,
                @SerializedName("email") val email: String
        ) : Dto()

        data class MovieResponse(
                @SerializedName("id") val id: Int,
                @SerializedName("title") val title: String,
                @SerializedName("description") val description: String?,
                @SerializedName("image") val image: String?,
                @SerializedName("adult") val adult: Boolean
        ) : Dto()

        data class ImageResponse(
                @SerializedName("key") val key: String,
                @SerializedName("email") val email: String
        ) : Dto()
    }
}