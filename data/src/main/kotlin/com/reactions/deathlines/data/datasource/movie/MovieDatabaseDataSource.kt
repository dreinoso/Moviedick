package com.reactions.deathlines.data.datasource.movie

import androidx.paging.DataSource
import com.reactions.deathlines.data.datasource.BaseDataSource
import com.reactions.deathlines.domain.entity.Entity

/**
 * Album database data source
 */
interface MovieDatabaseDataSource : BaseDataSource {

    fun getMovies(): DataSource.Factory<Int, Entity.Movie>

    fun persist(albums: List<Entity.Movie>, insertFinished: () -> Unit)

    fun clearTable(insertFinished: () -> Unit)
}