package com.reactions.deathlines.data.datasource.movie

import androidx.paging.DataSource
import com.reactions.deathlines.data.db.album.MovieDao
import com.reactions.deathlines.data.mapper.map
import com.reactions.deathlines.domain.entity.Entity
import java.util.concurrent.Executor

class MovieDatabaseDataSourceImpl(private val movieDao: MovieDao,
                                  private val ioExecutor: Executor) : MovieDatabaseDataSource {

    override fun getMovies(): DataSource.Factory<Int, Entity.Movie> =
            movieDao.selectAllPaged()
                    .map { it.map() }

    override fun persist(movie: List<Entity.Movie>, insertFinished: () -> Unit) {
        ioExecutor.execute {
            movieDao.insert/*persist*/(movie.map { it.map() })
            insertFinished()
        }
    }

    override fun clearTable(insertFinished: () -> Unit) {
        ioExecutor.execute {
            movieDao.clearTable()
        }
    }
}