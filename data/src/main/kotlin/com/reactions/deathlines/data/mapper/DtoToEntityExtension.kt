package com.reactions.deathlines.data.mapper

import com.com.reactions.deathlines.data.api.MoviesApi
import com.reactions.deathlines.domain.entity.Entity

/**
 * Extension class to map album dto to album entity
 */
fun MoviesApi.Dto.MovieResponse.map() = Entity.Movie(
        id = id,
        title = title,
        description = description ?: "",
        image = image ?: ""
)