package com.reactions.deathlines.data.repository.preferences

import android.content.Context
import android.content.SharedPreferences
import com.reactions.deathlines.domain.repository.PreferenceRepository

class PreferenceRepositoryImpl(
        context: Context
) : PreferenceRepository {
    private val PREF_KEY_ACCESS_TOKEN = "PREF_KEY_ACCESS_TOKEN"
    private val PREFERENCE_FILE_NAME = "DEFAULT"

    private var mPrefs: SharedPreferences? = null

    init {
        mPrefs = context.getSharedPreferences(PREFERENCE_FILE_NAME, Context.MODE_PRIVATE)

    }

    override fun getAccessToken(): String {
        @Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
        return mPrefs!!.getString(this.PREF_KEY_ACCESS_TOKEN, "")
    }

    override fun setAccessToken(accessToken: String) {
        mPrefs!!.edit().putString(PREF_KEY_ACCESS_TOKEN, accessToken).apply()
    }
}