package com.reactions.deathlines.data.datasource.movie

import android.annotation.SuppressLint
import io.reactivex.Single
import com.reactions.deathlines.data.datasource.BaseDataSource
import com.reactions.deathlines.domain.common.ResultState
import com.reactions.deathlines.domain.entity.Entity

@SuppressLint("CheckResult")
fun getMovies(
        apiSource: AlbumsApiDataSource,
        page: Int,
        sort: String = "",
        querySearch: String = "",
        onResult: (result: ResultState<List<Entity.Movie>>) -> Unit
) {
    apiSource.getMovies(page, sort, querySearch)
            .subscribe({ data ->
                onResult(ResultState.Success(data))
            }, { throwable ->
                onResult(ResultState.Error(throwable, null))
            })
}

interface AlbumsApiDataSource : BaseDataSource {

    fun getApiKey(email: String): Single<String>

    fun getMovies(page: Int,
                  sort: String = "",
                  querySearch: String = ""
    ): Single<List<Entity.Movie>>
}