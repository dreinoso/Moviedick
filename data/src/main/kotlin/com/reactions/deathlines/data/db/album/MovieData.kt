package com.reactions.deathlines.data.db.album

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

sealed class MovieData {

    @Entity(tableName = "movies_table")
    data class Movie(@ColumnInfo(name = "id") @PrimaryKey(autoGenerate = false) val id: Long,
                     @ColumnInfo(name = "title") val title: String,
                     @ColumnInfo(name = "description") val description: String,
                     @ColumnInfo(name = "image") val image: String) : MovieData()
}