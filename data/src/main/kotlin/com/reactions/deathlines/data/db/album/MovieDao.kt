package com.reactions.deathlines.data.db.album

import androidx.paging.DataSource
import androidx.room.Dao
import androidx.room.Query
import io.reactivex.Flowable
import com.reactions.deathlines.data.db.BaseDao

@Dao
interface MovieDao : BaseDao<MovieData.Movie> {

    @Query("SELECT * FROM movies_table WHERE id = :id")
    override fun select(id: Long): Flowable<MovieData.Movie>

    @Query("SELECT * FROM movies_table ORDER BY id")
    override fun selectAllPaged(): DataSource.Factory<Int, MovieData.Movie>

    @Query("DELETE FROM movies_table")
    override fun truncate()

    @Query("DELETE FROM movies_table WHERE id BETWEEN :firstId AND :lastId")
    fun deleteRange(firstId: Long, lastId: Long)

    @Query("DELETE FROM movies_table")
    fun clearTable()
}