package com.reactions.deathlines.data.mapper

import org.junit.Assert.*
import org.junit.Test
import com.reactions.deathlines.domain.entity.Entity

class DataToEntityExtensionKtTest {

    val id = 1234
    val title = "Avenger"
    val description = "Tony stark <3"

    val movieEntity = Entity.Movie(
            id = id,
            title = title,
            description = description,
            image = ""
    )

    @Test
    fun mapFromEntity() {
        val movieData = movieEntity.map()
        assertEquals(movieData.title, title)
        assertEquals(movieData.description, description)
    }
}