package com.reactions.deathlines.data.repository.album

import com.reactions.deathlines.data.BaseNetworkTest
import com.reactions.deathlines.data.datasource.movie.MovieApiDataSourceImpl
import com.reactions.deathlines.data.repository.preferences.PreferenceRepositoryImpl

import org.junit.Test;

class MoviesApiDataSourceTest : BaseNetworkTest() {

private lateinit var movieRepositoryTest: MovieApiDataSourceImpl
private lateinit var preferenceRepositoryTest: PreferenceRepositoryImpl


    override fun setUp() {
        super.setUp()
        movieRepositoryTest = MovieApiDataSourceImpl(moviesApi, preferenceRepositoryTest)
    }


    @Test
    fun validateGetMovies() {
        try {
            val response = movieRepositoryTest.getMovies(0)
            checkNotNull(response)
        } catch (e: Exception) {
            checkNotNull(e.message)
        }
    }

}