package com.reactions.deathlines.data

import com.com.reactions.deathlines.data.api.MoviesApi

import org.junit.Before
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

abstract class BaseNetworkTest {

    lateinit var moviesApi: MoviesApi

    val amountFake = "123"
    val emailFake = "jorge@quebuu.com"

    @Before
    open fun setUp() {
        val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("https://dev-candidates.wifiesta.com/")
                .build()
        moviesApi = retrofit.create(MoviesApi::class.java)
    }

}
