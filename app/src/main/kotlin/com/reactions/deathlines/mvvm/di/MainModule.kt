package com.reactions.deathlines.mvvm.di

import dagger.Module
import dagger.android.ContributesAndroidInjector
import com.reactions.deathlines.mvvm.di.home.HomeFragmentModule
import com.reactions.deathlines.presentation.ui.MainActivity
import com.reactions.deathlines.presentation.ui.login.LoginActivity

@Module(includes = [HomeFragmentModule::class])
abstract class MainModule {

    //@PerActivity
    @ContributesAndroidInjector
    abstract fun getMainActivity(): MainActivity

    //@PerActivity
    @ContributesAndroidInjector
    abstract fun getLoginActivity(): LoginActivity
}