package com.reactions.deathlines.mvvm.di.home

import android.content.Context
import dagger.Module
import dagger.Provides
import com.com.reactions.deathlines.data.api.MoviesApi
import com.reactions.deathlines.data.datasource.movie.AlbumsApiDataSource
import com.reactions.deathlines.data.datasource.movie.MovieApiDataSourceImpl
import com.reactions.deathlines.data.datasource.movie.MovieDatabaseDataSource
import com.reactions.deathlines.data.datasource.movie.MovieDatabaseDataSourceImpl
import com.reactions.deathlines.data.db.album.MovieDao
import com.reactions.deathlines.data.repository.album.MoviesRepositoryImpl
import com.reactions.deathlines.data.repository.preferences.PreferenceRepositoryImpl
import com.reactions.deathlines.domain.repository.PreferenceRepository
import com.reactions.deathlines.domain.repository.album.MoviesRepository
import com.reactions.deathlines.domain.usecase.album.*
import com.reactions.deathlines.domain.usecase.album.preferences.GetApiKeyInPreferencesUseCase
import com.reactions.deathlines.domain.usecase.album.preferences.GetApiKeyInPreferencesUseCaseImpl
import com.reactions.deathlines.domain.usecase.album.preferences.SetApiKeyInPreferencesUseCase
import com.reactions.deathlines.domain.usecase.album.preferences.SetApiKeyInPreferencesUseCaseImpl
import com.reactions.deathlines.mvvm.di.qualifier.ApplicationContext
import java.util.concurrent.Executors

@Module
class HomeModule {

    @Provides
    //@PerFragment
    fun provideDatabaseSource(albumDao: MovieDao): MovieDatabaseDataSource =
            MovieDatabaseDataSourceImpl(albumDao, Executors.newSingleThreadExecutor())

    @Provides
    //@PerFragment
    fun provideApiSource(
            api: MoviesApi,
            preferenceRepository: PreferenceRepository
    ): AlbumsApiDataSource = MovieApiDataSourceImpl(api, preferenceRepository)

    @Provides
    //@PerFragment
    fun provideRepository(
            apiSource: AlbumsApiDataSource,
            databaseSource: MovieDatabaseDataSource
    ): MoviesRepository {
        return MoviesRepositoryImpl(apiSource, databaseSource)
    }

    @Provides
    //@PerFragment
    fun providePreferenceRepository(
            @ApplicationContext context: Context
    ): PreferenceRepository {
        return PreferenceRepositoryImpl(context)
    }

    @Provides
    fun provideGetMoviesFromAlbumUseCaseImpl(repository: MoviesRepository): GetMoviesUseCase = GetMoviesUseCaseImpl(repository)

    @Provides
    fun provideGetApiKeyUseCaseImpl(repository: MoviesRepository): GetApiKeyUseCase = GetApiKeyUseCaseImpl(repository)

    @Provides
    fun provideSetApiKeyInPreferencesUseCaseImpl(repository: PreferenceRepository): SetApiKeyInPreferencesUseCase =
            SetApiKeyInPreferencesUseCaseImpl(repository)

    @Provides
    fun provideGetApiKeyInPreferencesUseCaseImpl(repository: PreferenceRepository): GetApiKeyInPreferencesUseCase =
            GetApiKeyInPreferencesUseCaseImpl(repository)
}
