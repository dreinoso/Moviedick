package com.reactions.deathlines.mvvm.di.home

import com.reactions.deathlines.presentation.ui.albumdetail.MovieDetailFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector
import com.reactions.deathlines.presentation.ui.home.HomeFragment
import com.reactions.deathlines.presentation.ui.login.LoginFragment

@Module
abstract class HomeFragmentModule {

    @ContributesAndroidInjector
    abstract fun homeFragment(): HomeFragment

    @ContributesAndroidInjector
    abstract fun AlbumDetailFragment(): MovieDetailFragment

    @ContributesAndroidInjector
    abstract fun LoginFragment(): LoginFragment
}