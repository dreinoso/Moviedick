package com.reactions.deathlines.presentation.ui.albumdetail

import android.os.Bundle
import android.text.method.ScrollingMovementMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.reactions.deathlines.presentation.R
import com.reactions.deathlines.presentation.databinding.FragmentMovieDetailBinding
import com.reactions.deathlines.presentation.ui.base.BaseFragment
import com.squareup.picasso.Callback
import com.squareup.picasso.OkHttp3Downloader
import com.squareup.picasso.Picasso
import okhttp3.OkHttpClient
import okhttp3.Request
import java.lang.Exception
import javax.inject.Inject


class MovieDetailFragment : BaseFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: FragmentMovieDetailBinding

    private val viewModel: MovieDetailsViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(MovieDetailsViewModel::class.java)
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        binding = FragmentMovieDetailBinding.inflate(inflater, container, false)
        val bindingUnmutable = binding!!
        return bindingUnmutable.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        showLoading()
        initViews()
    }

    private fun initViews() {
        binding.btnDone.setOnClickListener(::onDoneBtnClicked)
        binding.tvTitle.text = arguments?.get("title").toString()
        binding.tvDescription.text = arguments?.get("description").toString()
        binding.tvDescription.movementMethod = ScrollingMovementMethod()
        loadIamge()
    }

    private fun loadIamge() {
        val builder = Picasso.Builder(this.context!!)
        builder.downloader(OkHttp3Downloader(
                OkHttpClient.Builder()
                        .addInterceptor { chain ->
                            val newRequest: Request = chain.request().newBuilder()
                                    .addHeader(viewModel.apiHeader, viewModel.getApiKey())
                                    .build()
                            chain.proceed(newRequest)
                        }
                        .build()
        ))
        val imagePath = arguments?.get("image").toString()
        builder.build().load(viewModel.imageApiPath  + imagePath)
                .error(R.drawable.ic_baseline_movie_filter_24)
                .into(binding.ivImage, object: Callback {
                    override fun onSuccess() {
                        hideLoading()
                    }

                    override fun onError(e: Exception?) {
                        hideLoading()
                    }
                })
    }

    fun onDoneBtnClicked(view: View) {
        activity?.onBackPressed()
    }
}