package com.reactions.deathlines.presentation.ui.login

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.reactions.deathlines.domain.common.ResultState
import com.reactions.deathlines.presentation.R
import com.reactions.deathlines.presentation.common.extension.observe
import com.reactions.deathlines.presentation.databinding.FragmentLoginBinding
import com.reactions.deathlines.presentation.ui.MainActivity
import com.reactions.deathlines.presentation.ui.base.BaseFragment
import javax.inject.Inject

class LoginFragment : BaseFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private var isLoading = false
    private var _binding: FragmentLoginBinding? = null
    private val binding get() = _binding!!


    private val viewModel: LoginViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(LoginViewModel::class.java)
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentLoginBinding.inflate(inflater, container, false)
        return binding.root
    }

    private fun onApiLoaded(resultState: ResultState<String>) {
        Log.d(tag, "onApiLoaded resultState: $resultState")
        when (resultState) {
            is ResultState.Success -> {
                hideLoading()
                startActivity(Intent(context, MainActivity::class.java))
            }
            is ResultState.Error -> {
                hideLoading()
                Toast.makeText(activity, resultState.throwable.message, Toast.LENGTH_LONG).show()
            }
        }
        isLoading = false
    }

    @SuppressLint("CheckResult")
    private fun initView() {
        binding.btnLogin.setOnClickListener { onClickLoginBtn() }
    }

    fun onClickLoginBtn() {
        val email = binding.etEmail.text.toString()
        if(email.isBlank()) {
            Toast.makeText(activity, getString(R.string.no_email_error), Toast.LENGTH_LONG).show()
        } else {
            closeKeyboard()
            viewModel.setEmail(email)
            showLoading()
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        observe(viewModel.apiKeyLiveData, ::onApiLoaded)
    }

    fun closeKeyboard() {
        try {
            val inputManager: InputMethodManager = (context)?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputManager.hideSoftInputFromWindow(activity?.currentFocus?.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
        } catch (e: Exception) {
            Log.e(tag, e.message)
        }
    }
}