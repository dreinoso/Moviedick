package com.reactions.deathlines.presentation.ui.albumdetail

import com.reactions.deathlines.domain.usecase.album.preferences.GetApiKeyInPreferencesUseCase
import com.reactions.deathlines.presentation.ui.base.BaseViewModel
import javax.inject.Inject

class MovieDetailsViewModel @Inject constructor(
        private val getApiKeyInPreferencesUseCase: GetApiKeyInPreferencesUseCase) : BaseViewModel() {

    val apiHeader = "api-key"
    val imageApiPath = "https://dev-candidates.wifiesta.com/images"

    fun getApiKey(): String {
        return getApiKeyInPreferencesUseCase.getApiKeyInPreferencesUseCase()
    }
}