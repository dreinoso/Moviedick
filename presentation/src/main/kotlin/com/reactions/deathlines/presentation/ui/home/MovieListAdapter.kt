package com.reactions.deathlines.presentation.ui.home

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.reactions.deathlines.domain.entity.Entity
import com.reactions.deathlines.presentation.R
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.item_song.view.*

class MovieListAdapter (
        private val movieClickedListener: MovieClickedListener
) : PagedListAdapter<Entity.Movie, MovieListAdapter.MovieViewHolder>(MovieDiffCallback()) {

    private val onAlbumItemClickSubject = PublishSubject.create<Entity.Movie>()
    val albumItemClickEvent: Observable<Entity.Movie> = onAlbumItemClickSubject

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_song, parent, false)
        return MovieViewHolder(view)
    }

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        val album = getItem(position)
        album?.let { holder.bind(album) }
    }

    inner class MovieViewHolder(private var itemAlbumBinding: View) : RecyclerView.ViewHolder
    (itemAlbumBinding), View.OnClickListener {
        private val root = itemAlbumBinding.cv_root
        private val title = itemAlbumBinding.tv_title
        private val context = itemAlbumBinding.context

        fun bind(movieItem: Entity.Movie) {
            title.text = movieItem.title
            root.setOnClickListener(this)
        }

        override fun onClick(view: View) {
            val movieClicked = getItem((adapterPosition))
            movieClicked?.let {
                movieClickedListener.onSongClicked(movieClicked)
            }
        }
    }

    interface MovieClickedListener {
        fun onSongClicked(movie : Entity.Movie)
    }
}