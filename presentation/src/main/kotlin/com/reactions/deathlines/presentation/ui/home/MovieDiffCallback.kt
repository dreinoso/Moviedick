package com.reactions.deathlines.presentation.ui.home

import androidx.recyclerview.widget.DiffUtil
import com.reactions.deathlines.domain.entity.Entity

class MovieDiffCallback : DiffUtil.ItemCallback<Entity.Movie>() {

    override fun areItemsTheSame(oldItem: Entity.Movie, newItem: Entity.Movie): Boolean =
            oldItem.id == newItem.id && oldItem.title == newItem.title

    override fun areContentsTheSame(oldItem: Entity.Movie, newItem: Entity.Movie): Boolean =
            oldItem == newItem && oldItem.title == newItem.title

    override fun getChangePayload(oldItem: Entity.Movie, newItem: Entity.Movie): Any? {
        // Return particular field for changed item.
        return super.getChangePayload(oldItem, newItem)
    }
}