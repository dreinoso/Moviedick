package com.reactions.deathlines.presentation.ui.home

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.paging.PagedList
import io.reactivex.disposables.Disposable
import com.reactions.deathlines.domain.common.ResultState
import com.reactions.deathlines.domain.entity.Entity
import com.reactions.deathlines.domain.usecase.album.GetMoviesUseCase
import com.reactions.deathlines.presentation.common.OperationLiveData
import com.reactions.deathlines.presentation.ui.base.BaseViewModel
import javax.inject.Inject

class HomeViewModel @Inject constructor(private val getMoviesUseCase: GetMoviesUseCase) : BaseViewModel() {

    private val SORT_TYPES = listOf("title.asc", "title.desc", "date.asc", "date.desc", "popularity.asc", "popularity.desc")
    private var sortType = ""
    private var searchTitle = ""
    private var tempDisposable: Disposable? = null
    private val fetch = MutableLiveData<String>()

    val moviesLiveData: LiveData<ResultState<PagedList<Entity.Movie>>> = Transformations.switchMap(fetch) {
        OperationLiveData<ResultState<PagedList<Entity.Movie>>> {
            if (tempDisposable?.isDisposed != true)
                tempDisposable?.dispose()
            tempDisposable = getMoviesUseCase.getMovies(sortType, searchTitle).subscribe { resultState ->
                postValue((resultState))
            }
            tempDisposable?.track()
        }
    }

    fun getMovies(title: String) {
        searchTitle = title
        fetch.postValue(title)
    }

    fun setSortType(index: Int) {
        sortType = SORT_TYPES[index]
        fetch.postValue(sortType)
    }
}