package com.reactions.deathlines.presentation.ui.login

import android.os.Bundle
import androidx.fragment.app.Fragment
import com.reactions.deathlines.presentation.R
import com.reactions.deathlines.presentation.ui.base.BaseActivity

class LoginActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
    }

    override fun getNavControllerId(): Int = R.id.activityMainHomeHostFragment
}