package com.reactions.deathlines.presentation.ui.login

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import io.reactivex.disposables.Disposable
import com.reactions.deathlines.domain.common.ResultState
import com.reactions.deathlines.domain.usecase.album.GetApiKeyUseCase
import com.reactions.deathlines.domain.usecase.album.preferences.SetApiKeyInPreferencesUseCase
import com.reactions.deathlines.presentation.common.OperationLiveData
import com.reactions.deathlines.presentation.ui.base.BaseViewModel
import javax.inject.Inject

class LoginViewModel @Inject constructor(
        private val getApiKeyUseCase: GetApiKeyUseCase,
        val setApiKeyInPreferencesUseCase: SetApiKeyInPreferencesUseCase) : BaseViewModel() {

    private var tempDisposable: Disposable? = null
    private val emailLiveData = MutableLiveData<String>()

    val apiKeyLiveData: LiveData<ResultState<String>> = Transformations.switchMap(emailLiveData) {
        OperationLiveData<ResultState<String>> {
            if (tempDisposable?.isDisposed != true)
                tempDisposable?.dispose()
            Log.d(this.javaClass.name, "getMovies: with ${emailLiveData.value}")

            tempDisposable = getApiKeyUseCase.getApiKey(emailLiveData.value!!).subscribe { resultState ->
                postValue(resultState)
                if (resultState is ResultState.Success) {
                    setApiKeyInPreferencesUseCase.setApiKeyInPreferencesUseCase(resultState.data)
                }
            }
            tempDisposable?.track()
        }
    }

    fun setEmail(email: String) {
        emailLiveData.postValue(email)
    }
}