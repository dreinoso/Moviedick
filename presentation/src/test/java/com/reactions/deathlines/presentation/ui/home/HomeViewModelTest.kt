package com.reactions.deathlines.presentation.ui.home

import com.reactions.deathlines.domain.usecase.album.GetImageUseCase
import com.reactions.deathlines.presentation.ui.base.BaseUnitTest
import org.junit.Before
import org.junit.Test

import org.junit.Assert.*
import org.mockito.Mock

class HomeViewModelTest : BaseUnitTest() {

    @Mock
    private lateinit var getImageUseCase: GetImageUseCase

    private lateinit var homeViewModel : HomeViewModel

    @Before
    override fun setUp() {
        super.setUp()
        homeViewModel = HomeViewModel(getImageUseCase)
    }

    @Test
    fun getSongs() {
            homeViewModel.getMovies(testString)
            assertNotNull(homeViewModel.moviesLiveData)
        }
}