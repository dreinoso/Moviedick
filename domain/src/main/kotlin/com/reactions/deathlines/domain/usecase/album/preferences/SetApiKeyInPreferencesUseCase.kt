package com.reactions.deathlines.domain.usecase.album.preferences

import com.reactions.deathlines.domain.common.ResultState
import com.reactions.deathlines.domain.entity.Entity
import com.reactions.deathlines.domain.usecase.BaseUseCase
import io.reactivex.Single

interface SetApiKeyInPreferencesUseCase : BaseUseCase {

    fun setApiKeyInPreferencesUseCase(email: String)

}