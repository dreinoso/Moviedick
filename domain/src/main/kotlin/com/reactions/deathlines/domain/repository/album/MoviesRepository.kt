package com.reactions.deathlines.domain.repository.album

import androidx.paging.PagedList
import io.reactivex.Flowable
import io.reactivex.Single
import com.reactions.deathlines.domain.common.ResultState
import com.reactions.deathlines.domain.entity.Entity
import com.reactions.deathlines.domain.repository.BaseRepository

interface MoviesRepository : BaseRepository {

    fun getApiKey(email: String): Single<ResultState<String>>

    fun getMovies(sort: String, querySearch: String): Flowable<ResultState<PagedList<Entity.Movie>>>

}