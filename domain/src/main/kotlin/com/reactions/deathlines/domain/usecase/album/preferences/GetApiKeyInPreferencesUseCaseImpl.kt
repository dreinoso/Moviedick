package com.reactions.deathlines.domain.usecase.album.preferences

import com.reactions.deathlines.domain.repository.PreferenceRepository

class GetApiKeyInPreferencesUseCaseImpl(
        private val preferenceRepository: PreferenceRepository) : GetApiKeyInPreferencesUseCase {

    override fun getApiKeyInPreferencesUseCase(): String {
        return preferenceRepository.getAccessToken()
    }
}
