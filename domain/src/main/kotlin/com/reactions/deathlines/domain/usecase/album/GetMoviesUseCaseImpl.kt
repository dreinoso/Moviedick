package com.reactions.deathlines.domain.usecase.album

import androidx.paging.PagedList
import io.reactivex.Flowable
import com.reactions.deathlines.domain.common.ResultState
import com.reactions.deathlines.domain.entity.Entity
import com.reactions.deathlines.domain.repository.album.MoviesRepository

class GetMoviesUseCaseImpl(
        private val repository: MoviesRepository) : GetMoviesUseCase {

    override fun getMovies(sort: String, querySearch: String): Flowable<ResultState<PagedList<Entity.Movie>>> =
            repository.getMovies(sort, querySearch)
}