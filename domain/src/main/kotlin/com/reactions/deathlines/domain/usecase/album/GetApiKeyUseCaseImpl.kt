package com.reactions.deathlines.domain.usecase.album

import com.reactions.deathlines.domain.common.ResultState
import com.reactions.deathlines.domain.repository.album.MoviesRepository
import io.reactivex.Single

class GetApiKeyUseCaseImpl(
        private val repository: MoviesRepository) : GetApiKeyUseCase {

    override fun getApiKey(email: String): Single<ResultState<String>> =
            repository.getApiKey(email)
}