package com.reactions.deathlines.domain.repository

interface PreferenceRepository {
    fun getAccessToken(): String

    fun setAccessToken(accessToken: String)
}