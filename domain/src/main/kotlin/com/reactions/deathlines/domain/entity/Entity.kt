package com.reactions.deathlines.domain.entity

/**
 * Album entity
 */
sealed class Entity {

    data class Movie(
            val id: Int,
            val title: String,
            val description: String,
            val image: String
    ) : Entity()

    data class ApiKey(
            val apiKey: Int
    ) : Entity()
}