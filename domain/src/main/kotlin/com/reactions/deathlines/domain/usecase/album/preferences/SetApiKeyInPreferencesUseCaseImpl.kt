package com.reactions.deathlines.domain.usecase.album.preferences

import com.reactions.deathlines.domain.repository.PreferenceRepository

class SetApiKeyInPreferencesUseCaseImpl(
        private val preferenceRepository: PreferenceRepository) : SetApiKeyInPreferencesUseCase {

    override fun setApiKeyInPreferencesUseCase(apiKey: String) {
        preferenceRepository.setAccessToken(apiKey)
    }
}
